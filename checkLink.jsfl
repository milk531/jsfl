﻿//检查是否有重复定义
		var dom=fl.getDocumentDOM();
		var itemArray=dom.library.items;
		var links=[];
		for (var i=0;i<itemArray.length;i++)
		{
			var item=itemArray[i];
			if(item.itemType=="movie clip" && item.linkageExportForAS)
			{
				var name=item.linkageClassName;
				if(links.indexOf(name)==-1)
				{
					links.push(name);
				}
				else
				{
					fl.trace(name+"已存在，重复元件为'"+item.name+"'");
				}
			}
		}