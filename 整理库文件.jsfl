﻿//整理库
var doc=fl.getDocumentDOM();
doc.library.newFolder("bmp"); 
doc.library.newFolder("font"); 
doc.library.newFolder("graphic"); 
doc.library.newFolder("symbol"); 
doc.library.newFolder("helper"); 

doc.library.newFolder("symbol/_btn");
doc.library.newFolder("symbol/_bg");
doc.library.newFolder("symbol/_icon");
var items=doc.library.items;
for(var i=0;i<items.length;i++)
{
	var item=items[i];
	if(item.itemType=="bitmap")
	{
		doc.library.moveToFolder("bmp",item.name);
	}
	else if(item.itemType=="graphic")
	{
		doc.library.moveToFolder("graphic",item.name);
	}
	else if(item.itemType=="font")
	{
		doc.library.moveToFolder("font",item.name);
	}
	else if(item.itemType=="movie clip")
	{
		if(item.name.toLowerCase().indexOf("btn")!=-1)
			doc.library.moveToFolder("symbol/_btn",item.name);
		else if(item.name.toLowerCase().indexOf("bg")!=-1)
			doc.library.moveToFolder("symbol/_bg",item.name);
		else if(item.name.toLowerCase().indexOf("icon")!=-1)
			doc.library.moveToFolder("symbol/_icon",item.name);
		else
			doc.library.moveToFolder("symbol",item.name);
	}
}