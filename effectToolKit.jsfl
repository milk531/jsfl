﻿var input = fl.browseForFolderURL("选择输入"); 
var folderList=FLfile.listFolder(input);
while(folderList.length!=0)
{
	var fileName=folderList.pop();
	if(fileName.indexOf(".fla")==-1)
		continue;
	var ps=fileName.indexOf("(");
	if(ps==-1)
		ps=fileName.indexOf("（");
	var pe=fileName.indexOf(")");
	if(pe==-1)
		pe=fileName.indexOf("）");
	var pm=fileName.indexOf(",");
	if(pm==-1)
		pm=fileName.indexOf("，");
	var output=fileName.substring(0,ps);
	var px=fileName.substring(ps+1,pm);
	var py=fileName.substring(pm+1,pe);
	var fileURI=input+"/"+fileName;
	var doc=fl.openDocument(fileURI);
	var timeline= fl.getDocumentDOM().getTimeline();
	timeline.setSelectedLayers(0);
	timeline.addNewLayer("action","normal");
	timeline.layers[0].frames[0].actionScript = "var pivotx:int="+px+";var pivoty:int="+py+";";
	doc.exportSWF(input+"/"+output);
	fl.closeDocument(doc,false);
}

