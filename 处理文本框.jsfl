﻿
function setTextAttr(attrName,attrValue)
{
	var items = fl.getDocumentDOM().library.items;
	for (var i in items)//遍历所有库元件
	{
		var item = items[i] ;
		var itemType = item.itemType ;
	
		if (itemType == "movie clip" || itemType == "button" || itemType =="graphic")//只有图形元件和影片元件里含有文本
		{
			var layers = item.timeline.layers ;
			for (var j in layers)//遍历图层
			{
				var frames = layers[j].frames ;
				for (var k in frames)//遍历所有帧
				{
					var elements = frames[k].elements ;
					for (var m in elements)//遍历所有帧里的元素
					{
						var ele = elements[m] ;
						if (ele.elementType == "text")
						{
							ele.setTextAttr(attrName,attrValue);
							
							//ele.fontRenderingMode="device";
							//ele.selectable=false;
							//ele.scrollable=false;
							//ele.textType=static|dynamic|input;
						}
					}
				}
			}
		}	
	}
}
replaceText();
function extractText() 
{
	var doc=fl.getDocumentDOM();
	var langText = FLfile.read( doc.pathURI.replace(".fla",".txt")); 
	if(langText==null)
		langText="";
	var items = doc.library.items;
	
	for (var i in items)//遍历所有库元件
	{
		var item = items[i] ;
		var itemType = item.itemType ;
	
		if (itemType == "movie clip" || itemType == "button" || itemType =="graphic")//只有图形元件和影片元件里含有文本
		{
			var layers = item.timeline.layers ;
			for (var j in layers)//遍历图层
			{
				var frames = layers[j].frames ;
				for (var k in frames)//遍历所有帧
				{
					var elements = frames[k].elements ;
					for (var m in elements)//遍历所有帧里的元素
					{
						var ele = elements[m] ;
						if (ele.elementType == "text")
						{
							var key=item.name+i+j+k+m;
							
							if(!ele.hasPersistentData("uuid"))
							{
								var value=ele.getTextString();
								if(value)
								{
									ele.setPersistentData("uuid","string",key);
									langText+=(key+"="+value+";\n");
								}
							}
						}
					}
				}
			}
		}
	}
	FLfile.write(doc.pathURI.replace(".fla",".txt"),langText);	
	fl.trace("提取完毕");
}
function replaceText()
{
	var langDic={};
	var doc=fl.getDocumentDOM();
	var str = FLfile.read( doc.pathURI.replace(".fla",".txt")); 
	var kvs=str.split(";\n");
	for(var index in kvs)
	{
		var kv=kvs[index];
		if(kv)
		{
			var kva=kv.split("=");
			if(kva[0])
			{
				langDic[kva[0]]=kva[1];
			}
		}
	}
	var items = doc.library.items;
	
	for (var i in items)//遍历所有库元件
	{
		var item = items[i] ;
		var itemType = item.itemType ;
	
		if (itemType == "movie clip" || itemType == "button" || itemType =="graphic")//只有图形元件和影片元件里含有文本
		{
			var layers = item.timeline.layers ;
			for (var j in layers)//遍历图层
			{
				var frames = layers[j].frames ;
				for (var k in frames)//遍历所有帧
				{
					var elements = frames[k].elements ;
					for (var m in elements)//遍历所有帧里的元素
					{
						var ele = elements[m] ;
						if (ele.elementType == "text")
						{
							if(ele.hasPersistentData("uuid"))
							{
								var key=ele.getPersistentData("uuid");
								var value=langDic[key];
								if(value)
								{
									ele.setTextString(value);
								}
							}
						}
					}
				}
			}
		}
	}
	fl.trace("替换完毕");
}
function listFont()
{
	var fontList=[];
	var items = fl.getDocumentDOM().library.items;
	for (var i in items)//遍历所有库元件
	{
		var item = items[i] ;
		var itemType = item.itemType ;
	
		if (itemType == "movie clip" || itemType == "button" || itemType =="graphic")//只有图形元件和影片元件里含有文本
		{
			var layers = item.timeline.layers ;
			for (var j in layers)//遍历图层
			{
				var frames = layers[j].frames ;
				for (var k in frames)//遍历所有帧
				{
					var elements = frames[k].elements ;
					for (var m in elements)//遍历所有帧里的元素
					{
						var ele = elements[m] ;
						if (ele.elementType == "text")
						{
							var face=ele.getTextAttr("face");
							var index=fontList.indexOf(face);
							if(index==-1)
							{
								fontList.push(face);
							}
						}
					}
				}
			}
		}
	}
	fl.trace("Following Font is used: "+fontList);
}
