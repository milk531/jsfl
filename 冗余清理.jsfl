﻿fl.outputPanel.clear();

var checkStage=false;

var doc=fl.getDocumentDOM();

var lib=doc.library;
var items=lib.items;
var useCount={};

//检查舞台
if(checkStage)
{
	var timelines=doc.timelines;
	for(i=0;i<timelines.length;i++)
	{
		checkTimeline(timelines[i]);
	}
}

//检查所有库元件
for(i=0;i<items.length;i++)
{
	var item = items[i] ;
	if(item.itemType=="folder")
		continue;
	if(item.linkageExportForAS)
	{
		useCount[i]=true;
		checkItem(item);
	}
}
for(i=items.length-1;i>=0;i--)
{
	var item = items[i];
	if(item.itemType=="folder"||item.itemType=="font")
		continue;
	if(!useCount[i])
	{
		fl.trace(item.name+" is no longer used.");
		lib.deleteItem(item.name)
	}
}
function checkItem(item)
{
	var itemType=item.itemType;
	if (itemType == "movie clip" || itemType == "button" || itemType =="graphic")//元件
	{
		checkTimeline(item.timeline);
	}
}
function checkTimeline(timeline)
{
	var layers = timeline.layers ;
	for (var j in layers)//遍历图层
	{
		var frames = layers[j].frames ;
		for (var k in frames)//遍历所有帧
		{
			var elements = frames[k].elements ;
			for (var m in elements)//遍历所有帧里的元素
			{
				var ele = elements[m] ;
				if (ele.libraryItem)
				{
					useCount[lib.findItemIndex(ele.libraryItem.name)]=true;
					checkItem(ele.libraryItem);
				}
			}
		}
	}
}

