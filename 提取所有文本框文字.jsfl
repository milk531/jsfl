﻿var doc=fl.getDocumentDOM();
var items = doc.library.items;
var dic={};
var profile=doc.getSWFPathFromProfile();
var index=profile.lastIndexOf("/");
if(index==-1)
	index=profile.lastIndexOf("\\");
var fileName=profile.substring(index+1).replace(".swf","");
for (var i in items)//遍历所有库元件
{
	var item = items[i] ;
	var itemType = item.itemType ;
	
	if (itemType == "movie clip" || itemType == "button" || itemType =="graphic")//只有图形元件和影片元件里含有文本
	{
		var layers = item.timeline.layers ;
		for (var j in layers)//遍历图层
		{
			var frames = layers[j].frames ;
			for (var k in frames)//遍历所有帧
			{
				var elements = frames[k].elements ;
				for (var m in elements)//遍历所有帧里的元素
				{
					var ele = elements[m] ;
					if (ele.elementType == "text")
					{
						if(!ele.name)
						{
							var key=fileName+j+k+m;
							dic[key]=ele.getTextString();
							ele.name=key;
						}
					}
				}
			}
		}
	}	
}
var langText="";
for(key in dic)
{
	langText+="<String name=\""+key+"\">"+dic[key]+"</String>\n";
}
FLfile.write(doc.pathURI.replace(".fla",".txt"),langText);