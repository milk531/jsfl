﻿//保持文件夹结构导入图片
var inputFolder = fl.browseForFolderURL("Select Folder");
var dom=fl.getDocumentDOM();
dom.getTimeline().addNewLayer("label") ;
var count=0;
if(inputFolder)
{
	parseFolder(inputFolder);
}
function parseFolder(folder)
{
	var fileList=FLfile.listFolder(folder,"files");
	while(fileList.length!=0)
	{
		var filePath=fileList.shift();
		
		if(filePath.indexOf(".png")==-1 && filePath.indexOf(".jpg")==-1)
			continue;
		
		dom.importFile(folder+"/"+filePath,true);
		var fn=folder.replace(inputFolder,"").replace("/","");
		if(fn)
			dom.library.newFolder(fn);
		dom.library.selectItem(filePath);
		dom.library.moveToFolder(fn);
		dom.library.selectItem(fn+"/"+filePath);
		
		dom.getTimeline().currentLayer = 1; 
		if(count!=0)
			dom.getTimeline().insertBlankKeyframe(count);
			
		dom.library.addItemToDocument({x:0,y:0});
		var lastIndex=count-1;
		if(lastIndex<0)
			lastIndex=0;
		var label=dom.getTimeline().layers[0].frames[lastIndex].name;
		if(label!=fn)
		{
			dom.getTimeline().currentLayer = 0; 
			label=fn;
			if(count!=0)
				dom.getTimeline().insertKeyframe(count); 
			
			dom.getTimeline().layers[0].frames[count].name=label;
		}
		else
		{
			dom.getTimeline().currentLayer = 0; 
			dom.getTimeline().insertFrames(1 , false , count)
		}
		count++;
	}
	var folderList=FLfile.listFolder(folder,"directories");
	while(folderList.length!=0)
	{
		var folderName=folderList.shift();
		parseFolder(folder+"/"+folderName);
	}
}
function getFileName(path)
{
	var start=path.lastIndexOf("/");
	return path.slice(start+1);
}

